﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {

	// the transform of the object to follow
	public Transform target;

	private UnityEngine.AI.NavMeshAgent agent;

	void Start() {
		// get a reference to the NavMeshAgent component
		agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
	}	

	void Update () {
		// tell the agent to go to the target's position
		agent.SetDestination(target.position);
	}

}
